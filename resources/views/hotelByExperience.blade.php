@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="panel panel-success">
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @foreach ($hotels as $hotel)

                        <div class="product">
                            <div class="product-image">
                                <a href="{{ url('/detailHotel?id=') }}{{ $hotel["id"] }}">
                                
                                    <h1>{{ $hotel["name"] }}</h1>
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection




