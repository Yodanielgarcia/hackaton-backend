@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
                <a href="{{url('/home')}}">Regresar</a>
                <div class="">
                    <br>
                    <br>
                    <h3>Experiencias</h3>
                    <div class="experiences">
                        <div class="row">
                    @foreach ( $experiences as $experience)
                            <div class="col-sm-4" style="border: solid 1px lightgray !important">
                                <div class="">
                                    <h5 class="">
                                        <b>{{$experience['name']}}</b>
                                    </h5>
                                    
                                    <a href="{{url('/verAllExp/'.$experience['id'].'')}}"><img class="" style="border:solid 1px light-gray; width: 270px; height:200px" src="./images/3.jpg"></a>
                                    <p class="" style="height: 100px;"></p>
                                </div>
                            </div>
                    @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




