@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
                <a href="{{url('/home')}}">Regresar</a>
                <div class=""><h1>{{ $product['name'] }}</h1></div>
                <div class="">
                    {{$product['description']}}
                    <br>
                    <br>
                    <br>
                    <h3>Experiencias</h3>
                    <div class="experiences">
                    @foreach ( $experiences as $experience)
                        <div class="row">
                        @foreach ($experience as $item)
                            <div class="col-sm-4" style="border: solid 1px lightgray !important">
                                <div class="">
                                    <h5 class="">
                                        <b>{{ $item['experience_name'] }}</b>
                                    </h5>
                                    <!-- <img class="" style="margin:0 !important" src="./images/{{ $item['image'] }}"> -->
                                    <img class="hotelImage" style="border:solid 1px light-gray;" src="./images/c{{ $item['image'] }}">
                                    <p class="" style="height: 100px;">{{ $item['description'] }}</p>
                                    @if($item['isLiked'])
                                    <a href="{{ url('/user/') }}/{{$user}}/unlike/{{ $item["experience_id"] }}/hotel/{{ $product['product_id'] }}">
                                        <img src="./images/icon-loved.png" style="width: 20px; height: 20px;">
                                    </a>
                                    @else
                                    <a href="{{ url('/user/') }}/{{$user}}/like/{{ $item["experience_id"] }}/hotel/{{ $product['product_id'] }}">
                                        <img src="./images/icon-love.png" style="width: 20px; height: 20px;">
                                    </a>
                                    @endIf
                                </div>
                            </div>
                        @endforeach
                        </div>


                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




