@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="panel panel-success">
                        
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{url('/verGlobalExp')}}"  class="btn btn-primary">Explora</a>
                    @foreach ($products as $product)

                        <div class="product">
                            <div class="product-image">
                                <a href="{{ url('/detailHotel?id=') }}{{ $product["product_id"] }}">
                                    <img class="bigImage" src="./images/{{ $product["images"] }}" >
                                </a>
                            </div>
                            <div class="product-info">
                                <h1>{{ $product["name"] }}</h1>
                                <p>{{ $product["type_of_product"] }}</p>
                                <p>{{ $product["description"] }}</p>
                                @if ($product["recomended"])
                                <div class="recomendation">
                                    <a>
                                        <span>Recomended</span>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection




