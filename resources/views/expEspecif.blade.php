@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
                <a href="{{url('/verGlobalExp')}}">Regresar</a>
                <div class="">
                    <br>
                    <br>
                    <h3>Experiencias</h3>
                    <?= Form::open(['action' => 'GraphHotelController@verHotelesByExp', 'method' => 'POST']); ?>
                    <div class="experiences">
                        <div class="row">
                    @foreach ( $experiences as $experience)
                            <div class="col-sm-4" style="border: solid 1px lightgray !important">
                                <div class="">
                                    <h5 class="">
                                        <b>{{$experience['name']}}</b>
                                    </h5>
                                    <?=Form::checkbox($experience['name'], $experience['id']);?>

                                    <p class="" style="height: 100px;"></p>
                                </div>
                            </div>
                    @endforeach
                        </div>
                    </div>
                    <br>
                    <?=Form::button(
    '<span class="glyphicon glyphicon-search">Buscar</span>',
    array(
        'class'=>'btn btn-primary',
        'type'=>'submit')); Form::close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




