<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view("welcome");
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detailHotel', 'HomeController@detailHotel')->name('detailHotel');
Route::get('/verAllExp/{globalId}', 'GraphExperienceController@verAllExp')->name('verAllExp');
Route::get('/verGlobalExp', 'GraphExperienceController@verGlobalExp')->name('verGlobalExp');
Route::post('/verHotelesByExp', 'GraphHotelController@verHotelesByExp')->name('verHotelesByExp');
Route::resources(['hotels' => 'GraphHotelController']);
Route::resources(['experiences' => 'GraphExperienceController']);
Route::resources(['clients' => 'GraphClientController']);
Route::get('/hotels/{hotel}/experiences',"GraphHotelController@hotelExperiences");
Route::get('/user/{user}/like/{experience}/hotel/{hotel}',"GraphHotelController@registerLike")->name("like");
Route::get('/user/{user}/unlike/{experience}/hotel/{hotel}',"GraphHotelController@registerUnlike")->name("like");
Route::get('/user/{user}/like',"GraphHotelController@getUserLikes");
Route::post('/user/{user}/recomendations',"GraphHotelController@getRecomendations");
Route::get('/user/{user}/removeLike',"GraphHotelController@removeAllLikes");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/login/{provider}', 'SocialController@redirectToProvider')->name('redirect');
//Route::get('/login/{provider}/callback', 'SocialController@handleProviderCallback')->name('callback');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
