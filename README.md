# Decatalogo - Sistema de recomendaciones

Sistema de recomendaciones de productos hoteleros basado en las necesidades del cliente.
# Rutas disponibles
- Ruta para obtener los hoteles con su id, nombre, descipción y ids de las imagenes
```sh
GET http://127.0.0.1:8000/hotels
```
```javascript
[
    {
        "hotelId": 32,
        "nombreHotel": "Casa San Pedro",
        "descripcionHotel": "Rerum quas distinctio et dolor fugiat. Id exercitationem possimus nostrum eum. Aliquam ea iste officia repudiandae aut dolorem sit.",
        "numImg": "1,2,3"
    },
    ...
]
```
- Obtener todas las experiencias existentes en BD Grafica
```sh
GET http://127.0.0.1:8000/experiences
```
```javascript
[
    {
        "id": 109,
        "name": "Adecuación discapacitados",
        "description": "Aliquid eligendi sint dignissimos officia impedit voluptatem non. Eos aspernatur mollitia itaque vel ut. Et aliquid itaque iusto minus aliquid.",
        "imageId": 39
    },
    ...
]
```
- Obtencion de clientes existentes en la BD de graficas
```sh
GET http://127.0.0.1:8000/clients
```
```javascript
[
    {
        "id": 109,
        "name": "Adecuación discapacitados",
        "description": "Aliquid eligendi sint dignissimos officia impedit voluptatem non. Eos aspernatur mollitia itaque vel ut. Et aliquid itaque iusto minus aliquid.",
        "imageId": 39
    },
    ...
]
```
- Obtencion de experiencias que ofrece determinado hotel
```sh
GET http://localhost:8000/hotels/34/experiences
```
```javascript
[
    {
        "id": 74,
        "name": "Cena a la carta",
        "description": "Et et vitae adipisci quidem et nisi. Nulla est et minus. Dolorem iste harum eos. Accusamus in est quo veritatis quia non.",
        "imageId": 10
    },
    ...
]
```
- Registrar un LIKE de un usuario(1) a una experiencia(83)
```sh
POST http://localhost:8000/user/1/like/83
```
```javascript
{
    "registered": true
}
```
- Obtener las experiencias a las que le ha dado like un usuario
```sh
GET http://localhost:8000/user/1/like
```
```javascript
[
    {
        "id": 82,
        "name": "Centro convenciones",
        "description": "Sint eius possimus at enim. Et ad id optio consequuntur. Quo qui molestiae expedita voluptatem tempora corporis sapiente. Rem aut aut voluptas ad dicta voluptas aliquid."
    },
    {
        "id": 83,
        "name": "Lago",
        "description": "Numquam cumque quae debitis. Temporibus iure nulla rem enim dolore id quasi. Autem dolorum iusto omnis est fuga ea et. Et a quia sint sapiente."
    },
    {
        "id": 81,
        "name": "Mascotas",
        "description": "Vitae nostrum ullam sint sit officiis. Et ex voluptates aut autem qui voluptas. Maiores illum nisi quia. Sunt quis corrupti necessitatibus dolor. Molestias quibusdam mollitia aperiam."
    }
]
```
- Query
```neo4j
MATCH (h1:Hotel)-[:OFFER]->(c1:caracteristica)<-[:OFFER]-(h2:Hotel)
WITH h1, h2, count(c1) as NCaracteristicas, collect(c1) as carComunes
WHERE NCaracteristicas > 3
RETURN NCaracteristicas, h1.name as Hotel1, extract(x in carComunes|x.name) as carComunes, h2.name as HotelRecomendado
ORDER BY NCaracteristicas desc
limit 10


MATCH (p1:Person)-[:LIKE]->(car:caracteristica)<-[:OFFER]-(h1:Hotel)-[:OFFER]->(car2:caracteristica)<-[:OFFER]-(h2:Hotel)
WITH p1, h1, h2, count(car) as cantCarComunes, collect(car) as carComunes, count(car2) as cantCarComunes2, collect(car2) as carComunes2 
WHERE p1.id = '1'

RETURN distinct h1.name //cantCarComunes, p1.name as Persona, h1.name as Hotel1, extract(x in carComunes|x.name) as carComunes, h2.name as hotelRecomendado, cantCarComunes2, extract(x in carComunes2|x.name) as carComunes2
//ORDER BY cantCarComunes desc
limit 10


validacion grafica

MATCH (p1:Person)-[:LIKE]->(car:caracteristica)<-[:OFFER]-(h1:Hotel)-[:OFFER]->(car2:caracteristica)<-[:OFFER]-(h2:Hotel)

WHERE p1.id = '1'
RETURN p1, car, h1, h2, car2
```


License
----

MIT
