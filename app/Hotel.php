<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{

    protected $fillable = [
        'description', 'hotel', 'img',
    ];

    protected $table = 'hotel';
    protected $primaryKey = 'idHotel';
    public $incrementing = true;
}
