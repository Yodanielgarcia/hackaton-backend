<?php

namespace App\Business;
use App\Transport\GraphDB;
use App\Util\Util;

class Grafos {
    private $client;

    public function __construct()
    {
        $this->client = GraphDB::getClient();
    }

    public function listAllHotels()
    {
        $query = 'MATCH (hotel:Hotel) RETURN hotel';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $hotels = [];
        foreach($records as $record){
            array_push($hotels, [
                'name' => $record->value("hotel")->value("name"),
                'id' => $record->value("hotel")->value("id")
            ]);
        }
        return $hotels;
    }

    public function listHotelsByExperiences($experiences)
    {
        //$experiences = implode(",", $experiences);
        $array1 = array();
        $array = array();
        foreach($experiences as $exp){
                array_push($array1, $exp);
        }
        $array = array_shift($array1);
        $var = implode(",", $array1);
        $query = 'MATCH (hotel:Hotel)-[o:OFFER]->(e:caracteristica)
         WHERE e.id IN['.$var.'] RETURN distinct hotel.id, hotel.name';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $hotels = [];
        foreach($records as $record){
            array_push($hotels, [
                'name' => $record->value("hotel.name"),
                'id' => $record->value("hotel.id")
            ]);
        }
        return $hotels;
    }

    public function addHotel($hotelData)
    {
        $document = Util::genDocument($hotelData);
        $query = 'CREATE ( h_'.$hotelData['id'].':Hotel '.$document.' ) RETURN h_'.$hotelData['id'].'.id';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        foreach($records as $record){
            return $record->value("h_".$hotelData['id'].".id");
        }
        return false;
    }

    public function relationToHotel($hotelExperienceData)
    {
        //$document = Util::genDocument($hotelOfferExperienceData);
        $hotelId = $hotelExperienceData['hotel'];
        $experienceId = $hotelExperienceData['experience'];
        $query = 'MATCH
                        (h:Hotel { id: "'.$hotelId.'" }),
                        (e:caracteristica {id: "'.$experienceId.'"})
                    CREATE (h)-[r:OFRECE]->(e)
                    RETURN h, e, r';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        dd($records);
        /*foreach($records as $record){
            return $record->value("e_".$experienceData['id'].".id");
        }*/
        return false;
    }

    public function listAllExperiences($globalId)
    {
        $query = 'MATCH (experience:caracteristica)<-[o:OFFER]
        -(h:Hotel)<-[n:GLOBAL]-(g:global)
         WHERE g.id='.$globalId.'
         RETURN distinct experience.id, experience.name';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $experiences = [];
        foreach($records as $record){
            array_push($experiences, [
                'name' => $record->value("experience.name"),
                'id' => $record->value("experience.id")
            ]);
        }
        return $experiences;
    }

    public function listGlobalExperiences()
    {
        $query = 'MATCH (experience:global)
        RETURN distinct experience.id, experience.name
        order by experience.name';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $experiences = [];
        foreach($records as $record){
            array_push($experiences, [
                'name' => $record->value("experience.name"),
                'id' => $record->value("experience.id")
            ]);
        }
        return $experiences;
    }

    public function listAllClients()
    {
        $query = 'MATCH (person:Person) RETURN person';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $clients = [];
        foreach($records as $record){
            array_push($clients, [
                'name' => $record->value("person")->value("name"),
                'id' => $record->value("person")->value("id")
            ]);
        }
        return $clients;
    }

    public function addExperience($experienceData)
    {
        $document = Util::genDocument($experienceData);
        $query = 'CREATE ( h_'.$experienceData['id'].':caracteristica '.$document.' ) RETURN e_'.$experienceData['id'].'.id';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        foreach($records as $record){
            return $record->value("e_".$experienceData['id'].".id");
        }
        return false;
    }

    public function listHotelExperiences($hotelId)
    {
        $query = 'MATCH (experience:caracteristica)<-[o:OFFER]-(n:Hotel)
                  WHERE n.id = '.$hotelId.'
                  RETURN experience.id, experience.name, o.imageId
                  order by experience.name';
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        $experiences = [];
        foreach($records as $record){
            array_push($experiences, [
                'name' => $record->value("experience.name"),
                'id' => $record->value("experience.id"),
                'idImage' => $record->value("o.imageId")
            ]);
        }
        return $experiences;
    }

    public function registerUserLikeExperience($user, $experience)
    {
        $isAlreadyLiked = $this->isAlreadyLiked($user, $experience);
        if(!$isAlreadyLiked) {
            $query = "MATCH
                        (person:Person { id : '".$user."'}),
                        (experience:caracteristica { id : ".$experience."})
                    CREATE (person)-[r:LIKE {code:'1'}]->(experience)
                    RETURN person,experience,r";
            \Log::debug($query);
            $result = $this->client->run($query);
            $records = $result->getRecords();
            foreach($records as $record){
                if($record->value("r")->value("code") == "1"){
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    public function registerUserUnLikeExperience($user, $experience)
    {
        $isAlreadyLiked = $this->isAlreadyLiked($user, $experience);
        if($isAlreadyLiked) {
            $query = "MATCH (person:Person)-[r:LIKE]->(experience:caracteristica)
            WHERE person.id = '".$user."' and experience.id = ".$experience."
            DELETE r  RETURN count(r) as total";
            \Log::debug($query);
            $result = $this->client->run($query);
            $records = $result->getRecords();
            return true;
        }
        return true;
    }


    public function isAlreadyLiked($user, $experience)
    {
        $query = "MATCH (person:Person)-[r:LIKE]->(experience:caracteristica)
                  WHERE person.id = '".$user."' and experience.id = ".$experience."
                  return person, experience, r";
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        foreach($records as $record){
            if($record->value("r")->value("code") == "1"){
                return true;
            }
        }
        return false;
    }

    public function listUserLikesExperiences($userId)
    {
        $query = "MATCH (person:Person)-[o:LIKE]->(experience:caracteristica)
                  WHERE person.id = '".$userId."'
                  RETURN experience.id, experience.name, o.code
                  order by experience.name";
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();
        $experiences = [];
        foreach($records as $record){
            array_push($experiences, [
                'name' => $record->value("experience.name"),
                'id' => $record->value("experience.id")
            ]);
        }
        return $experiences;
    }

    public function getHotelRecomedations($user)
    {
        $query = "MATCH (p1:Person)-[:LIKE]->(car:caracteristica)<-[:OFFER]-(h1:Hotel)-[:OFFER]->(car2:caracteristica)<-[:OFFER]-(h2:Hotel)
        WITH p1, h1, h2, count(car) as cantCarComunes, collect(car) as carComunes, count(car2) as cantCarComunes2, collect(car2) as carComunes2
        WHERE p1.id = '".$user."'

        RETURN distinct h1.name, h1.id //cantCarComunes, p1.name as Persona, h1.name as Hotel1, extract(x in carComunes|x.name) as carComunes, h2.name as hotelRecomendado, cantCarComunes2, extract(x in carComunes2|x.name) as carComunes2
        ";
        \Log::debug($query);
        $result = $this->client->run($query);
        $records = $result->getRecords();

        $hotels = [];
        foreach($records as $record){
            array_push($hotels, [
                'name' => $record->value("h1.name"),
                'id' => $record->value("h1.id")
            ]);
        }
        return $hotels;
    }

    public function clearLikes($user)
    {
        $query = "MATCH (p:Person { id : '".$user."'})-[r:LIKE]->(e:caracteristica)
                  DELETE r  RETURN count(r) as total";
        \Log::debug($query);
        $result = $this->client->run($query);
        return true;
    }

}
