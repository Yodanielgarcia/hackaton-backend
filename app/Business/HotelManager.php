<?php

namespace App\Business;
use DB;

class HotelManager
{

    public function __construct()
    { }

    public function loadInfoHotels($hotelesIdArray)
    {
        $resultArray = [];
        $counter = 0;
        foreach ($hotelesIdArray as $hotelId) {
            \Log::critical($result = DB::table('hotel')->where('idHotel', $hotelId['id'])->toSql()."   -- ".$hotelId['id']);
            $result = DB::table('hotel')->where('idHotel', $hotelId['id'])->first();
            $resultArray[$counter]['hotelId'] = $result->idHotel;
            $resultArray[$counter]['nombreHotel'] = $result->hotel;
            $resultArray[$counter]['descripcionHotel'] = $result->description;
            $resultArray[$counter]['numImg'] = $result->img;
            $counter++;
        }
        return $resultArray;
    }

    public function loadInfoOtherHotels($hotelesIdArray)
    {
        $notIn = [];
        foreach ($hotelesIdArray as $hotelId) {
            $notIn[] = $hotelId['hotelId'];
        }
        $results = DB::table('hotel')->whereNotIn('idHotel', $notIn)->get();
        $resultArray = [];
        $counter = 0;
        foreach($results as $result){
            $resultArray[$counter]['hotelId'] = $result->idHotel;
            $resultArray[$counter]['nombreHotel'] = $result->hotel;
            $resultArray[$counter]['descripcionHotel'] = $result->description;
            $resultArray[$counter]['numImg'] = $result->img;
            $counter++;
        }
        return $resultArray;
    }

    public function loadInfoExperiences($experiencesArray)
    {
        $resultArray = [];
        $counter = 0;
        foreach ($experiencesArray as $experienceId) {

            \Log::critical($result = DB::table('experiencia')->where('id', $experienceId['id'])->toSql()."   -- ".$experienceId['id']);
            $result = DB::table('experiencia')->where('id', $experienceId['id'])->first();
            $resultArray[$counter]['id'] = $result->id;
            $resultArray[$counter]['name'] = $result->name;
            $resultArray[$counter]['description'] = $result->description;
            if(key_exists('idImage', $experienceId)){
                $resultArray[$counter]['imageId'] = $experienceId['idImage'];
            }
            $counter++;
        }
        return $resultArray;
    }

    public function joinResults($hotels, $otherHotel)
    {
        $allResults = [];
        $count = 0;
        foreach($hotels as $k => $hotel){
            $imagenes = $hotel["numImg"];
            $imagenes = explode(",", $imagenes);
            //$hotels[$k]["imagen"] = $imagenes[0].".jpg";
            $hotels[$k]["imagen"] = "1.jpg";
            $hotels[$k]["recomendation"] = true;
            $allResults[$count] = $hotels[$k];
            $count++;
        }

        foreach($otherHotel as $k => $hotel){
            $imagenes = $hotel["numImg"];
            $imagenes = explode(",", $imagenes);
            //$hotels[$k]["imagen"] = $imagenes[0].".jpg";
            $otherHotel[$k]["imagen"] = "1.jpg";
            $otherHotel[$k]["recomendation"] = false;

            $allResults[$count] = $otherHotel[$k];
            $count++;
        }

        return $allResults;
    }
}
