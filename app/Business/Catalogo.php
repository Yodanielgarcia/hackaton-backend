<?php

namespace App\Business;
use App\Transport\GraphDB;
use App\Util\Util;
use App\Business\Grafos;
use App\Business\HotelManager;
use DB;

class Catalogo {

    public function getProducts($userId, $search, $justOneImage = true)
    {
        $hotels =$this->getHotels($userId, $justOneImage);
        $explorer = $this->getExplorer($userId);
        return array_merge($hotels, $explorer);
    }

    private function getRecomendedHotelsIds($user)
    {
        $query = "MATCH (p1:Person)-[:LIKE]->(car:caracteristica)<-[:OFFER]-(h1:Hotel)-[:OFFER]->(car2:caracteristica)<-[:OFFER]-(h2:Hotel)
        WITH p1, h1, h2, count(car) as cantCarComunes, collect(car) as carComunes, count(car2) as cantCarComunes2, collect(car2) as carComunes2
        WHERE p1.id = '".$user."'
        RETURN distinct h1.name, h1.id //cantCarComunes, p1.name as Persona, h1.name as Hotel1, extract(x in carComunes|x.name) as carComunes, h2.name as hotelRecomendado, cantCarComunes2, extract(x in carComunes2|x.name) as carComunes2";
        \Log::debug($query);
        $client = GraphDB::getClient();
        $result = $client->run($query);
        $records = $result->getRecords();
        $recomendedHotels = [];
        foreach($records as $recomendation){
            $recomendedHotels[] = $recomendation->value("h1.id");
        }
        return $recomendedHotels;
    }
    private function getMyLikes($user)
    {
        $query = "MATCH (p:Person)-[:LIKE]->(c:caracteristica)  where p.id = '".$user."' return c.id";
        \Log::debug($query);
        $client = GraphDB::getClient();
        $result = $client->run($query);
        $records = $result->getRecords();
        $likes = [];
        foreach($records as $like){
            $likes[] = $like->value("c.id");
        }
        return $likes;
    }
    public function prepareHotels($results, $recomended, $justOneImage = true)
    {
        $return = [];
        foreach($results as $result){
            $imagenes = explode(",", $result->img);
            foreach($imagenes as $kimg => $val){
                $imagenes[$kimg] = $val.".jpg";
            }

            $return[] = [
                "product_id" => $result->idHotel,
                "type_of_product" => "Hotel",
                "name" => $result->hotel,
                "description" => $result->description,
                "images" => ($justOneImage) ? $imagenes[0] : $imagenes,
                "recomended" => $recomended
            ];
        }
        return $return;
    }
    private function getCaracteristics($idHotel)
    {
        $query = "MATCH (hotel:Hotel)-[o:OFFER]->(c:caracteristica)
        WHERE hotel.id = ".$idHotel."
        RETURN c.id, c.name, o.imageId
        ORDER BY c.name";
        \Log::debug($query);
        $client = GraphDB::getClient();
        $result = $client->run($query);
        $records = $result->getRecords();
        $caracter = [];
        foreach($records as $characteristic){
            $caracter[$characteristic->value("o.imageId")] = $characteristic->value("c.id");
        }
        return $caracter;
    }
    private function getHotels($user, $justOneImage = true)
    {
        $recomendedHotels = $this->getRecomendedHotelsIds($user);
        $userLikes = $this->getMyLikes($user);
        $results = DB::table('hotel')->whereIn('idHotel', $recomendedHotels)->get();
        $recomended = $this->prepareHotels($results, true, $justOneImage);

        $results = DB::table('hotel')->whereNotIn('idHotel', $recomendedHotels)->get();
        $notRecomended = $this->prepareHotels($results, false, $justOneImage);
        $hotels = array_merge($recomended, $notRecomended);
        return $hotels;
    }

    private function getExplorer()
    {
        return [];
    }

    private function prepareExperiences($results, $userLikes, $caracteristics)
    {
        $return = [];
        foreach($results as $result){
            $key = array_search($result->id, $caracteristics);
            $return[] = [
                "experience_id" => $result->id,
                "experience_name" => ucfirst($result->name),
                "description" => $result->description,
                "isLiked" => (in_array($result->id, $userLikes)) ? true : false,
                "image" => $key.".jpg"
            ];
        }
        return $return;
    }
    public function getProduct($user, $hotelId)
    {
        $results = DB::table('hotel')->whereIn('idHotel', [$hotelId])->get();
        $hotel = $this->prepareHotels($results, false, false);
        $caracteristics = $this->getCaracteristics($hotelId);

        $userLikes = $this->getMyLikes($user);
        $results = DB::table('experiencia')->whereIn('id', $caracteristics)->get();
        $experiences = $this->prepareExperiences($results, $userLikes, $caracteristics);
        $hotel[0]['experiences'] = $experiences;
        return $hotel[0];
    }

}
