<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business\Grafos;
use App\Business\HotelManager;

class GraphExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Grafos $graphExperience, HotelManager $hotelManager)
    {
        $experiences = $graphExperience->listAllExperiences();
        $experiencesResponse = $hotelManager->loadInfoExperiences($experiences);
        return response()->json($experiencesResponse);
    }

    public function verAllExp(Request $request, Grafos $graphExperience, HotelManager $hotelManager)
    {
        
        $experiences = $graphExperience->listAllExperiences($request['globalId']);
        $experiencesResponse = $hotelManager->loadInfoExperiences($experiences);
        return view('expEspecif')
            ->with("experiences", $experiencesResponse);
    }

    public function verGlobalExp(Request $request, Grafos $graphExperience, HotelManager $hotelManager)
    {
        $experiences = $graphExperience->listGlobalExperiences();
        return view('expGlobal')
            ->with("experiences", $experiences);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Grafos $graphExperience)
    {
        $experienceData = $request->input("data");
        $experienceId = $graphExperience->addExperience($experienceData);
        return $experienceId;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
