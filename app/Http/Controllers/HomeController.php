<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business\Grafos;
use App\Business\HotelManager;
use App\Business\Catalogo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Grafos $graphHotel, HotelManager $hotelManager)
    {
        $user = auth()->user()->id;
        $search = $request->input("search");
        $catalog = new Catalogo();
        $productos = $catalog->getProducts($user, $search);
        return view('home')
            ->with("search", $search)
            ->with("products", $productos);
    }

    public function detailHotel(Request $request){
        $user = auth()->user()->id;
        $hotelId = $request->input("id");
        $catalog = new Catalogo();
        $product = $catalog->getProduct($user, $hotelId);
        $e = [];
        $cantidad = 0;
        for($i = 0; $i < count($product["experiences"]) ; $i++ ){
            if( $i % 3 == 0){
                $cantidad++;
            }
            $e[$cantidad][] = $product["experiences"][$i];
        }
        return view('detailHotel')
            ->with("id", $request->input("id"))
            ->with("product", $product)
            ->with("images", $product["images"])
            ->with("user", $user)
            ->with("experiences", $e);
    }
}
