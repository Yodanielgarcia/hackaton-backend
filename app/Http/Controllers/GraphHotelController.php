<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business\Grafos;
use App\Business\HotelManager;
use App\Business\Catalogo;
use DB;

class GraphHotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Grafos $graphHotel, HotelManager $hotelManager)
    {
        $hotels = $graphHotel->listAllHotels();
        $hotelsResponse = $hotelManager->loadInfoHotels($hotels);
        return response()->json($hotelsResponse);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Grafos $graphHotel)
    {
        $hotelData = $request->input("data");
        $hotelId = $graphHotel->addHotel($hotelData);
        return $hotelId;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verHotelesByExp(Request $request, Grafos $graphExperience, HotelManager $hotelManager)
    {
        $hotels = $graphExperience->listHotelsByExperiences($request->post());
        $recomendedHotels = [];
        foreach($hotels as $recomendation){
            $recomendedHotels[] = $recomendation['id'];
        }
        $results = DB::table('hotel')->whereIn('idHotel', $recomendedHotels)->get();
        $catalog = new Catalogo();
        $recomended = $catalog->prepareHotels($results, true, true);
        /*return view('hotelByExperience')
            ->with("hotels", $hotels);*/
            return view('home')
            ->with("search", "")
            ->with("products", $recomended);
    }

    public function hotelExperiences(Request $request, $id, Grafos $graphHotel, HotelManager $experienceManager)
    {
        $experiences = $graphHotel->listHotelExperiences($id);
        $experiencesResponse = $experienceManager->loadInfoExperiences($experiences);
        return response()->json($experiencesResponse);
    }

    public function registerLike(Request $request, $user, $experience, $hotel, Grafos $graphHotel, HotelManager $experienceManager)
    {
        $experiences = $graphHotel->registerUserLikeExperience($user, $experience);
        return redirect(url('/detailHotel?id=').$hotel);
        //return response()->json(['registered' => $experiences]);
    }

    public function registerUnlike(Request $request, $user, $experience, $hotel, Grafos $graphHotel, HotelManager $experienceManager)
    {
        $experiences = $graphHotel->registerUserUnLikeExperience($user, $experience);
        return redirect(url('/detailHotel?id=').$hotel);
        //return response()->json(['registered' => $experiences]);
    }
    public function getUserLikes(Request $request, $user, Grafos $graphHotel, HotelManager $experienceManager)
    {

        $experiences = $graphHotel->listUserLikesExperiences($user);
        $experiencesResponse = $experienceManager->loadInfoExperiences($experiences);
        return response()->json($experiencesResponse);
    }

    public function getRecomendations(Request $request, $user, Grafos $graphHotel, HotelManager $hotelManager)
    {
        $hotels = $graphHotel->getHotelRecomedations($user);
        $hotelsResponse = $hotelManager->loadInfoHotels($hotels);
        return response()->json($hotelsResponse);
    }

    public function removeAllLikes(Request $request, $user, Grafos $grafos)
    {
        $elimino = $grafos->clearLikes($user);
        return redirect(url('/home'));
    }


    public function catalogo($user, Request $request, Grafos $graphHotel, HotelManager $hotelManager)
    {
        $hotels = $graphHotel->listAllHotels();
        $hotelsResponse = $hotelManager->loadInfoHotels($hotels);
        $products = [];
        $count = 0;
        $group = 0;
        foreach($hotelsResponse as $item){
            $count++;
            if($count==6){
                $count = 1;
                $group++;
            }
            $products[$group][] = $item;
        }
        //dd($products);
        return view('demo')->with('products', $products)->with('user', $user);
    }

    public function detalleCatalogo($user, $hotel, Request $request, Grafos $graphHotel, HotelManager $hotelManager)
    {
        $hotels = $graphHotel->listAllHotels();
        $hotelsResponse = $hotelManager->loadInfoHotels($hotels);
        $products = null;
        foreach($hotelsResponse as $item){
            if($item["hotelId"] != $hotel){
                continue;
            } else {
                $product = $item;
                break;
            }
        }
        return view('startdemo')->with('product', $product)->with('user', $user);
    }
}
