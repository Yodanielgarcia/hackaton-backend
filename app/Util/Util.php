<?php

namespace App\Util;

class Util {
    public static function genDocument($data)
    {
        $document = "";
        foreach($data as $index => $info){
            if($document != "")
                $document .= ", ";
            $document .= $index.': "'.$info.'"';
        }
        return "{".$document."}";
    }
}
