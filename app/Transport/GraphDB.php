<?php

namespace App\Transport;

use GraphAware\Neo4j\Client\ClientBuilder;


class GraphDB {


    public static function getClient()
    {
        $client = ClientBuilder::create()
        ->addConnection('default', 'http://'.env("DB_USERNAME_GRAFOS").':'.env("DB_PASSWORD_GRAFOS").'@localhost:7474') // Example for HTTP connection configuration (port is optional)
        ->addConnection('bolt', 'bolt://'.env("DB_USERNAME_GRAFOS").':'.env("DB_PASSWORD_GRAFOS").'@localhost:7687') // Example for BOLT connection configuration (port is optional)
        ->build();
        return $client;
    }
}
