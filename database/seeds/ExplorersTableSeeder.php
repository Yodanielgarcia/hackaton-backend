<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExplorersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $explorers = [
            [ "id" => "112", "name" => "safari"],
            [ "id" => "113", "name" => "observación de aves"],
            [ "id" => "114", "name" => "city tour"],
            [ "id" => "115", "name" => "buceo"],
            [ "id" => "116", "name" => "paseo en coche"],
            [ "id" => "117", "name" => "vuelta a la isla"],
            [ "id" => "118", "name" => "avistamiento de rayas"],
           
        ];
        foreach($explorers as $explorer){
            DB::table('explorers')->insert([
	            'id' =>   $explorer['id'],
	            'name' => $explorer['name'],
	            'description' => $faker->text
	        ]);
        }
    }
}
