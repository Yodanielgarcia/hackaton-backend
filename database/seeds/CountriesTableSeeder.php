<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $countries = [
            [ "id" => "1", "name" => "México"],
            [ "id" => "2", "name" => "Colombia"],
            [ "id" => "3", "name" => "Perú"],
            [ "id" => "4", "name" => "Ecuador"],
            [ "id" => "5", "name" => "Costa Rica"],
            [ "id" => "6", "name" => "Haití"],
            [ "id" => "7", "name" => "El Salvador"],
            [ "id" => "8", "name" => "Panamá"],
            [ "id" => "9", "name" => "Jamaica"],
        ];
        foreach($countries as $country){
            DB::table('countries')->insert([
	            'id' =>   $country['id'],
	            'name' => $country['name']
	        ]);
        }

    }
}
