<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cities = [
          [ "id" => "10", "name" => "San Andrés"],
          [ "id" => "11", "name" => "Cartagena"],
          [ "id" => "12", "name" => "Santa Marta"],
          [ "id" => "13", "name" => "Providencia"],
          [ "id" => "14", "name" => "Eje Cafetero"],
          [ "id" => "15", "name" => "Amazonas"],
          [ "id" => "16", "name" => "Boyacá"],
          [ "id" => "17", "name" => "Bogotá"],
          [ "id" => "18", "name" => "San José"],
          [ "id" => "19", "name" => "Esmeraldas"],
          [ "id" => "20", "name" => "Santa Elena"],
          [ "id" => "21", "name" => "Sonsonate"],
          [ "id" => "22", "name" => "Costa Arcadins"],
          [ "id" => "23", "name" => "Montego Bay"],
          [ "id" => "24", "name" => "Runaway Bay"],
          [ "id" => "25", "name" => "Bucerías"],
          [ "id" => "26", "name" => "Rincón de Guayabitos"],
          [ "id" => "27", "name" => "San José del Cabo"],
          [ "id" => "28", "name" => "Farallón"],
          [ "id" => "29", "name" => "Tumbes"],
          [ "id" => "30", "name" => "Santa Clara"],
          [ "id" => "31", "name" => "Lima"],
            
        ];
        foreach($cities as $city){
            DB::table('cities')->insert([
	            'id' =>   $city['id'],
	            'name' => $city['name']
	        ]);
        }

    }
}
