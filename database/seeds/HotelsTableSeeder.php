<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $hotels = [
            [ "id" => "32", "name" => "Casa San Pedro", "img" => "1,2,3"],
            [ "id" => "33", "name" => "Barú", "img" => "4,5,6"],
            [ "id" => "34", "name" => "cartagena", "img" => "7,8,9"],
            [ "id" => "35", "name" => "Galeón", "img" => "10,11,12"],
            [ "id" => "36", "name" => "Aquarium", "img" => "13,14,15"],
            [ "id" => "37", "name" => "Isleño", "img" => "16,17,18"],
            [ "id" => "38", "name" => "Maryland", "img" => "19,20,21"],
            [ "id" => "39", "name" => "Los Delfines", "img" => "22,23,24"],
            [ "id" => "40", "name" => "Mar Azul", "img" => "25,26,27"],
            [ "id" => "41", "name" => "San Luis", "img" => "28,29,30"],
            [ "id" => "42", "name" => "Cabañas Relax", "img" => "31,32,33"],
            [ "id" => "43", "name" => "Miss Elma", "img" => "34,35,36"],
            [ "id" => "44", "name" => "Miss Mary", "img" => "37,38,39"],
            [ "id" => "45", "name" => "Posada del Mar", "img" => "40,41,42"],
            [ "id" => "46", "name" => "Las Heliconias", "img" => "43,44,45"],
            [ "id" => "47", "name" => "Panaca", "img" => "46,47,48"],
            [ "id" => "48", "name" => "Decalodge Ticuna", "img" => "49,50,51"],
            [ "id" => "49", "name" => "Aloft", "img" => "52,53,54"],
            [ "id" => "50", "name" => "W Bogotá", "img" => "55,56,57"],
            [ "id" => "51", "name" => "Rancho Tota", "img" => "58,59,60"],
            [ "id" => "52", "name" => "Santa Inés", "img" => "61,62,63"],
            [ "id" => "53", "name" => "San José", "img" => "64,65,66"],
            [ "id" => "54", "name" => "Mompiche", "img" => "67,68,69"],
            [ "id" => "55", "name" => "Punta Centinela", "img" => "70,71,72"],
            [ "id" => "56", "name" => "Salinitas", "img" => "73,74,75"],
            [ "id" => "57", "name" => "Índigo Beach", "img" => "76,77,78"],
            [ "id" => "58", "name" => "Cornwall Beach", "img" => "79,80,81"],
            [ "id" => "59", "name" => "Montego Beach", "img" => "82,83,84"],
            [ "id" => "60", "name" => "Club Caribean", "img" => "85,86,87"],
            [ "id" => "61", "name" => "Complex", "img" => "88,89,90"],
            [ "id" => "62", "name" => "Los Cocos", "img" => "91,92,93"],
            [ "id" => "63", "name" => "Los Cabos", "img" => "94,95,96"],
            [ "id" => "64", "name" => "Panamá", "img" => "97,98,99"],
            [ "id" => "65", "name" => "Punta Sal", "img" => "100,101,102"],
            [ "id" => "66", "name" => "El Pueblo", "img" => "103,104,105"],
            [ "id" => "67", "name" => "Radisson Miraflores", "img" => "106,107,108"],
            [ "id" => "68", "name" => "Radisson San Isidro", "img" => "109,110,111"],

        ];
        foreach($hotels as $hotel){
            DB::table('hotel')->insert([
	            'idHotel' =>   $hotel['id'],
	            'hotel' => $hotel['name'],
	            'description' => $faker->text,
	            'img' => $hotel['img'],
	        ]);
        }
    }
}
