<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $hotels = [
            [ "id" => "69", "name" => "Playa"],
            [ "id" => "70", "name" => "Piscina"],
            [ "id" => "71", "name" => "Gimnasio"],
            [ "id" => "72", "name" => "parqueadero"],
            [ "id" => "73", "name" => "Cena buffet"],
            [ "id" => "74", "name" => "Cena a la carta"],
            [ "id" => "75", "name" => "Bosque"],
            [ "id" => "76", "name" => "Selva"],
            [ "id" => "77", "name" => "Cuidad"],
            [ "id" => "78", "name" => "Campestre"],
            [ "id" => "79", "name" => "Spa"],
            [ "id" => "80", "name" => "Masajes"],
            [ "id" => "81", "name" => "Mascotas"],
            [ "id" => "82", "name" => "Centro convenciones"],
            [ "id" => "83", "name" => "Lago"],
            [ "id" => "84", "name" => "Pesca deportiva"],
            [ "id" => "85", "name" => "Deportes náuticos no motorizados"],
            [ "id" => "86", "name" => "Navegación"],
            [ "id" => "87", "name" => "Salón de juegos"],
            [ "id" => "88", "name" => "Cancha tennis"],
            [ "id" => "89", "name" => "Cancha football"],
            [ "id" => "90", "name" => "Cancha voleyball"],
            [ "id" => "91", "name" => "Campo de Golf"],
            [ "id" => "92", "name" => "Zona infantil"],
            [ "id" => "93", "name" => "Cine"],
            [ "id" => "94", "name" => "Discoteca"],
            [ "id" => "95", "name" => "Cancha multiusos"],
            [ "id" => "96", "name" => "Cancha baloncesto"],
            [ "id" => "97", "name" => "Plan boda"],
            [ "id" => "98", "name" => "Plan luna de miel"],
            [ "id" => "99", "name" => "Plan quince años"],
            [ "id" => "100", "name" => "Renta carros"],
            [ "id" => "101", "name" => "Renta motos"],
            [ "id" => "102", "name" => "Boutique"],
            [ "id" => "103", "name" => "Fiestas temáticas"],
            [ "id" => "104", "name" => "karaoke"],
            [ "id" => "105", "name" => "Peluquería"],
            [ "id" => "106", "name" => "Sauna"],
            [ "id" => "107", "name" => "Club playa"],
            [ "id" => "108", "name" => "Sala videojuegos"],
            [ "id" => "109", "name" => "Adecuación discapacitados"],
            [ "id" => "110", "name" => "billar"],
            [ "id" => "111", "name" => "ping pong"]
        ];
        foreach($hotels as $hotel){
            DB::table('experiencia')->insert([
                'id' =>   $hotel['id'],
                'name' => $hotel['name'],
                'description' => $faker->text
            ]);
        }



    }
}
